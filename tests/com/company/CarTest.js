TestCase('com.company.CarTest', {

	testfullName : function() {
		var car = new Car('VW', 'Beatle', 1971);
		assertEquals('VW Beatle Y: 1971', car.getFullName());
	},

	testStopEngineWithCheck : function() {
		var car = new Car('VW', 'Beatle', 1971);
		assertEquals('engine was not running', car.stopEngineWithCheck());
	}
	}

});